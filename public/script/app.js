'use strict';

/**
 * @ngdoc overview
 * @name rhd
 * @description
 * # rhd
 *
 * Main module of the application.
 */

angular.module('rhd', [ 'ngMap', 'ui.router' ])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('map',{
            url: '/', 
            controller: 'MainCtrl',
            templateUrl: 'views/main.html'
        });


    $urlRouterProvider.otherwise('/');

  })

  .controller('MainCtrl', [ '$scope', '$http','NgMap', function ( $scope, $http, NgMap ) {

   $scope.positions = [];
    $http.get("script/data/csvdata.json")
      .then(function(response) {
          $scope.data = response.data;

          for (var i = 0, j = 0; i < $scope.data.length; i++) {

            if (i > 1000) { return;};

            if (!$scope.checkDupe($scope.data[i] , true) ) { 
               
                $scope.positions[j] = $scope.data[i];
                $scope.positions[j].ORD = 1; 
                j++; 

            };
             
          };

      });

      $scope.checkDupe = function (item , upd){
        for (var i = 0; i < $scope.positions.length; i++) {
          if ($scope.positions[i].BP == item.BP) {
            if (upd) {
              $scope.positions[i].ORD ++ ; 
            };
            return true;
          };
        };
        return false;
      }

      $scope.getIcon = function(val){
        if (val < 25) {
          return "../images/green25.ICO";
        } else if (val < 40) {
          return "../images/blue25.ICO";
        } else if (val < 100) {
          return "../images/orange25.ICO";
        } else if (val < 500) {
          return "../images/red25.ICO";
        } else if (val > 500) {
          return "../images/bahut25.ICO";
        };

      }

 

      $scope.modal = UIkit.modal("#item");

      $scope.showDetail = function(e, x){
        $scope.itemData = {};
        $scope.itemData = x;
        $scope.modal.show();  
      } 
  


  }]);
